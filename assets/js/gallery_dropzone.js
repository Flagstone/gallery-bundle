$(document).ready(() => {
  console.log('gallery dropzone JS loaded')

  let $dropzone = $('.gallery #dropzone')
  let $images = $('.gallery .upload')
  let $form = $('#gallery-form')
  let $formUuid = $('#gallery-form #gallery_form_uuid')

  $dropzone.on('click', (e) => {
    e.preventDefault()
    $images.click()
  })

  $dropzone.on('dragover', (e) => {
    e.preventDefault()
    e.stopPropagation()
    $dropzone.addClass('dragover')
  })
  $dropzone.on('dragleave', (e) => {
    e.preventDefault()
    e.stopPropagation()
    $dropzone.removeClass('dragover')
  })

  $('#gallery_form_files').on('change', (e) => {
    sendFiles(e.currentTarget.files)
  })

  $dropzone.on('drop', (e) => {
    e.preventDefault()
    e.stopPropagation()

    if (e.originalEvent.dataTransfer) {
      sendFiles(e.originalEvent.dataTransfer.files)
    }
    $dropzone.removeClass('dragover')
  })

  $('.gallery .delete-image-button').on('click', (e) => {
    let uuid = $(e.currentTarget).data('uuid')
    let url = $(e.currentTarget).data('url')

    deleteImage($(e.currentTarget), uuid, url)
  })

  $('.gallery .edit-image-button').on('click', (e) => {
    let uuid = $(e.currentTarget).data('uuid')
    let url = $(e.currentTarget).data('url')

    editImage($(e.currentTarget), uuid, url)
  })

  function deleteImage(element, uuid, url) {
    if (confirm('Do you confirm the removal of this image from the gallery ?')) {
      $.ajax({
        url: url,
        method: 'post',
        dataType: 'json',
        data: {uuid: uuid},
        success: (data) => {
          if ('ok' === data.status) {
            element.parent().parent().remove()
            $('#gallery-images-counter').text($('.gallery-images-container').length)
          }
        },
        error: () => {
          console.log('error')
        }
      })
    }
  }

  function editImage(element, uuid, url) {
    let editImageModal = $('#editImageModal')
    $.ajax({
      url: url,
      method: 'post',
      dataType: 'json',
      data: {uuid: uuid},
      success: (data) => {
        if ('ok' === data.status) {
          editImageModal.html('')
          editImageModal.append(data.html)
          editImageModal.show()
          $('<div class="modal-backdrop fade show" style="backdrop-filter: blur(2px)"></div>').appendTo('body')

          $(document).on('click', '#editImageModal .btn-close', function(e) {
            editImageModal.hide()
            $('.modal-backdrop').remove()
          });
          $(document).on('click', '#editImageModal .close', function(e) {
            editImageModal.hide()
            $('.modal-backdrop').remove()
          });
          $(document).on('click', '#editImageModal .save', function(e) {
            editImageModal.hide()
            $('.modal-backdrop').remove()
            /* appel Ajax pour enregistrer les données */
            let data = new FormData($('#gallery-image-form')[0])
            $.ajax({
              url: '/admin/gallery/save-gallery-image',
              method: 'post',
              dataType: 'json',
              data: data,
              processData: false,
              contentType: false,
              success: (data) => {
                if ('failed' === data.status) {
                  editImage(element, uuid, url)
                }
              },
              error: () => {
              }
            })
          })
        }
      },
      error: () => {
        console.log('error')
      }
    })
  }

  function sendFiles(files) {
    let nbFiles = files.length

    if (0 < nbFiles) {
      let $waitButton = $('<span>', {
        id: 'wait-button'
      })
      let $icon = $('<i>', {
        class: 'fa-solid fa-spinner fa-3x fa-spin'
      }).appendTo($waitButton)


      $('#dropzone p').hide()
      $('#dropzone').append($waitButton)

      let formData = new FormData()
      $.each(files, function(i, file) {
        formData.append(i, file)
      })
      formData.append('uuid', $formUuid.val())

      $.ajax({
        url: '/admin/gallery/send-tmp-files',
        method: 'post',
        dataType: 'json',
        data: formData,
        cache: false,
        contentType: false,
        processData: false,
        success: (data)  => {
          if (0 < data.nb_files) {
            let $buttonDiv = $('<div>', {
              class: 'd-flex flex-row justify-content-center w-100 position-absolute',
              id: 'buttons'
            })
            let $addButton = $('<button>', {
              type: 'button',
              text: 'Add files ('+ data.nb_files +')',
              class: 'btn btn-success'
            }).on('click', (e) => {
              let fileArray = data.files

              let $progressBar = $('<div>', {
                class: 'progress-bar'
              })
              let $uploadProgress = $('<div>', {
                class: 'upload-progress'
              }).appendTo($progressBar)
              $('<span>').appendTo($uploadProgress)

              $('#dropzone #buttons').remove()
              $('#dropzone').append($progressBar)

              saveFile(fileArray, 0)
            })

            let $cancelButton = $('<button>', {
              type: 'button',
              text: 'Cancel',
              class: 'ms-3 btn btn-danger'
            }).on('click', (e) => {
              e.stopPropagation()
              e.preventDefault()
              $buttonDiv.remove()
              showDragDrop()
            })

            $buttonDiv.append($addButton)
            $buttonDiv.append($cancelButton)
            $dropzone.append($buttonDiv)

            $('#dropzone span#wait-button').remove()
            $dropzone.off('click')
          }
        },
        errors: () => {
          console.log('errors')
        }
      })
    }
  }

  function saveFile(fileArray, fileIter) {

    $.ajax({
      url: '/admin/gallery/save-files',
      method: 'post',
      dataType: 'json',
      data: {'file': fileArray[fileIter], 'gallery_uuid': $dropzone.data('uuid')},
      success: (data) => {
        if ('ok' === data.status) {
          let newImage = data.image
          $('#gallery-images').append(newImage)
          $('#gallery-images-counter').text($('.gallery-images-container').length)

          $(document).on('click', '.delete-image-button[data-uuid="'+data.uuid+'"]', function(e) {
            let uuid = $(this).data('uuid');
            let url = $(this).data('url');
            deleteImage($(e.currentTarget), uuid, url)
          });
          $(document).on('click', '.edit-image-button[data-uuid="'+data.uuid+'"]', function(e) {
            let uuid = $(this).data('uuid');
            let url = $(this).data('url');
            editImage($(e.currentTarget), uuid, url)
          });
        }

        fileIter++
        let progress = Math.floor((fileIter / fileArray.length) * 100)
        $('#dropzone .upload-progress').css('width' , progress+'%')
        $('#dropzone .upload-progress span').text(progress+'%')
        if (fileIter < fileArray.length) {
          saveFile(fileArray, fileIter)
        } else {
          $('#dropzone .progress-bar').remove()
          showDragDrop()
        }

      },
      error: () => {

      }
    })
  }

  function showDragDrop() {
    $('#dropzone p').show()
    $dropzone.on('click', (e) => {
      e.preventDefault()
      $images.click()
    })
  }
})