<?php

namespace Flagstone\GalleryBundle\Controller;

use Flagstone\BaseEntityBundle\BaseEntity\Exception\SaverDeleteException;
use Flagstone\BaseEntityBundle\BaseEntity\Exception\SaverSaveException;
use Flagstone\BaseEntityBundle\BaseEntity\Saver;
use Flagstone\GalleryBundle\Entity\Gallery;
use Flagstone\GalleryBundle\Entity\GalleryImage;
use Flagstone\GalleryBundle\Repository\GalleryImageRepository;
use Flagstone\GalleryBundle\Repository\GalleryRepository;
use Flagstone\GalleryBundle\Type\GalleryFormType;
use Flagstone\GalleryBundle\Type\GalleryImageFormType;
use Flagstone\UuidGeneratorBundle\Uuid\UuidGenerator;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Form\FormInterface;
use Symfony\Component\HttpFoundation\File\File;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\Routing\Attribute\Route;
use Symfony\Component\Routing\Generator\UrlGeneratorInterface;
use ZipArchive;

#[Route(path: '/gallery', name: 'gallery_')]
class GalleryController extends AbstractController
{
    const AVAILABLE_IMAGE_EXTENSIONS = ['jpg', 'jpeg', 'png', 'gif', 'webp'];
    const TMP_PATH = '/public/uploads/tmp';

    #[Route(path: '/list', name: 'list')]
    public function list(GalleryRepository $galleryRepository): Response
    {
        $galleries = $galleryRepository->findAll();

        return $this->render(
            '@FlagstoneGallery/list.html.twig',
            [
                'base_admin'        => $this->getParameter('cms_base_admin_template'),
                'galleries'         => $galleries,
            ]
        );
    }

    #[Route(path: '/add', name: 'add')]
    public function add(): Response
    {
        $galleryForm = $this->createForm(GalleryFormType::class, new Gallery());
        return $this->renderPage($galleryForm, new Gallery());
    }

    #[Route(path: '/edit/{uuid}', name: 'edit')]
    public function edit(GalleryRepository $galleryRepository, string $uuid): Response
    {
        $gallery = $galleryRepository->findOneBy(['uuid' => $uuid]);

        if (null !== $gallery) {
            $galleryForm = $this->createForm(GalleryFormType::class, $gallery);
            return $this->renderPage($galleryForm, $gallery);
        }

        throw new NotFoundHttpException();
    }

    #[Route(path: '/save', name: 'save')]
    public function save(Request $request, GalleryRepository $galleryRepository, Saver $saver): Response
    {
        $formData = $request->request->all('gallery_form');

        if (!empty($formData['uuid'])) {
            $gallery = $galleryRepository->findOneBy(['uuid' => $formData['uuid']]);
        } else {
            $gallery = new Gallery();
        }

        $galleryForm = $this->createForm(GalleryFormType::class, $gallery);
        $galleryForm->handleRequest($request);

        if ($request->isMethod('POST') && $galleryForm->isSubmitted() && $galleryForm->isValid()) {
            try {
                $saver->save($gallery);
                $this->addFlash('success', 'gallery.save_successful');
                return $this->redirectToRoute('gallery_edit', ['uuid' => $gallery->getUuid()]);
            } catch (SaverSaveException $e) {
                $this->addFlash('danger', 'gallery.unable_to_save');
            }
        }
        return $this->redirectToRoute('gallery_edit', ['uuid' => $gallery->getUuid()]);
    }

    private function renderPage(FormInterface $form, Gallery $gallery): Response
    {
        return $this->render(
            '@FlagstoneGallery/edit.html.twig',
            [
                'base_admin'            => $this->getParameter('cms_base_admin_template'),
                'galleryForm'           => $form->createView(),
                'images'                => $gallery->getGalleryImages(),
                'uuid'                  => $gallery->getUuid(),
                'thumbnail_path'        => null !== $gallery->getUuid() ? $this->getThumbnailsUri($gallery->getUuid()) : null
            ]
        );
    }

    #[Route(path:'/send-tmp-files', name: 'send_tmp_files', methods: ['POST'])]
    public function addFilesToTmp(Request $request): JsonResponse
    {
        $zip = new ZipArchive();
        $files = $request->files->all();

        $this->removeFilesFromFolder($this->getTmpPath());
        $filesToBeAdd = [];
        $nbFiles = 0;

        /** @var UploadedFile $file */
        foreach($files as $file) {
            if ('application/zip' === $file->getClientMimeType()) {
                if (true === $zip->open($file->getPathname(), ZipArchive::CREATE)) {

                    for ($i = 0; $i < $zip->numFiles; $i++) {
                        $filename = $zip->getNameIndex($i);
                        $fileInfo = pathinfo($filename);

                        if (str_ends_with($filename, '/')) {
                            /* Is a directory */
                            if (!is_dir($this->getTmpPath() . '/' . $filename)) {
                                mkdir($this->getTmpPath() . '/' . $filename, 0777, true);
                            }
                        } else {
                            /* Is a file */
                            if (!is_dir($this->getTmpPath() . '/' . $fileInfo['dirname'])) {
                                mkdir($this->getTmpPath() . '/' . $fileInfo['dirname'], 0777, true);
                            }
                            /* Extract the file */
                            copy("zip://" . $file . "#" . $filename, $this->getTmpPath() . '/' . $filename);
                            $this->addImageFiles($filesToBeAdd, $filename);
                        }

                    }
                    $zip->close();
                }
            } else {
                $filename = $file->getClientOriginalName();
                $file->move($this->getTmpPath(), $filename);
                $this->addImageFiles($filesToBeAdd, $filename);
            }
        }

        return new JsonResponse(
            [
                'nb_files'  => count($filesToBeAdd),
                'files'     => $filesToBeAdd
            ]
        );
    }

    #[Route(path:'/save-files', name: 'save_file', methods: ['POST'])]
    public function saveFile(Request $request, UuidGenerator $generator, GalleryRepository $galleryRepository, GalleryImageRepository $galleryImageRepository, Saver $saver, UrlGeneratorInterface $urlGenerator): JsonResponse
    {
        $file = new File($this->getTmpPath().'/'.$request->request->get('file'));
        $galleryUuid = $request->request->get('gallery_uuid');

        /** @var Gallery $gallery */
        $gallery = $galleryRepository->findOneBy(['uuid' => $galleryUuid]);
        $imageExists = $galleryImageRepository->findOneBy(['galleryOriginalImageName' => $file->getFilename(), 'gallery' => $gallery]);

        if (null === $imageExists) {
            $imagick = new \Imagick();
            $imageName = $generator->generate();

            try {
                /**
                 * Generate Thumbnail image
                 */
                if (true === $imagick->readImage($file)) {
                    $width = $imagick->getImageWidth();

                    if ($width > $this->getThumbnailMaxWidth()) {
                        $imagick->scaleImage($this->getThumbnailMaxWidth(), 0);
                    }

                    $imagick->writeImage($this->getThumbnailsPath($galleryUuid).'/'.$imageName.'.'.$this->getThumbnailFormat());
                }
                /**
                 * Generate Image
                 */
                if (true === $imagick->readImage($file)) {
                    $width = $imagick->getImageWidth();

                    if ($width > $this->getImageMaxWidth()) {
                        $imagick->scaleImage($this->getImageMaxWidth(), 0);
                    }

                    $imagick->writeImage($this->getImagesPath($galleryUuid).'/'.$imageName.'.'.$this->getImageFormat());
                }

                $galleryImage = new GalleryImage();
                $galleryImage->setGallery($gallery);
                $galleryImage->setGalleryImageFilename($imageName.'.'.$this->getImageFormat());
                $galleryImage->setGalleryOriginalImageName($file->getFilename());
                $gallery->addGalleryImage($galleryImage);

                $saver->save($gallery);
                unlink($this->getTmpPath() . '/' . $file->getFilename());

            } catch (\ImagickException $e) {
                return new JsonResponse(
                    [
                        'status'    => 'failed',
                        'message'   => 'Unable to save image'
                    ]
                );
            }

            $imagePath = $this->getThumbnailsUri($galleryUuid).'/'.$galleryImage->getGalleryImageFilename();
            $imageName = $galleryImage->getGalleryOriginalImageName();
            $imageUuid = $galleryImage->getUuid();
            $imageDeleteUrl = $urlGenerator->generate('gallery_delete_gallery_image');
            $imageEditUrl = $urlGenerator->generate('gallery_edit_gallery_image');

            $imageTemplate = <<<HTML

                <div class="gallery-images-container col-12 col-md-2 p-2 d-flex justify-content-center">
                    <div class="d-flex justify-content-center align-items-center p-3" style="position: relative; border-radius: 25px; overflow: hidden; border: 1px gray dotted;">
                       <img src="$imagePath" alt="$imageName" style="width: auto; height: auto; max-width: 100%; max-height: 100%">
                        <div class="delete-image-button" data-uuid="$imageUuid" data-url="$imageDeleteUrl">
                            <i class="fa-solid fa-times"></i>
                        </div>
                        <div class="edit-image-button" data-uuid="$imageUuid" data-url="$imageEditUrl">
                            <i class="fa-solid fa-pen"></i>
                        </div>
                    </div>
                </div>
        HTML;

            return new JsonResponse(
                [
                    'status'    => 'ok',
                    'image'     => $imageTemplate,
                    'uuid'      => $imageUuid
                ]
            );
        }
        return new JsonResponse(
            [
                'status'    => 'failed',
                'message'   => 'Image with same name already exists in this gallery'
            ]
        );
    }

    #[Route(path: '/delete-gallery-image', name: 'delete_gallery_image', methods: 'POST')]
    public function deleteGalleryImage(Request $request, GalleryImageRepository $galleryImageRepository, Saver $saver): JsonResponse
    {
        $imageUuid = $request->request->get('uuid');

        if (null !== $imageUuid) {
            $galleryImage = $galleryImageRepository->findOneBy(['uuid' => $imageUuid]);
            if (null !== $galleryImage) {
                $imageFilename = $galleryImage->getGalleryImageFilename();
                $imageGalleryUuid = $galleryImage->getGallery()->getUuid();
                try {
                    $saver->delete($galleryImage);
                    unlink($this->getThumbnailsPath($imageGalleryUuid).'/'.$imageFilename);
                    unlink($this->getImagesPath($imageGalleryUuid).'/'.$imageFilename);

                    return new JsonResponse(
                        [
                            'status'    => 'ok'
                        ]
                    );
                } catch (SaverDeleteException $e) {
                    return new JsonResponse(
                        [
                            'status'    => 'failed',
                            'message'   => 'unable to delete image'
                        ]
                    );
                }
            }
        }

        return new JsonResponse(
            [
                'status'    => 'failed',
                'message'   => 'Image doesn\'t exists'
            ]
        );
    }

    #[Route(path: '/edit-gallery-image', name: 'edit_gallery_image', methods: 'POST')]
    public function editGalleryImage(Request $request, GalleryImageRepository $galleryImageRepository, Saver $saver): JsonResponse
    {
        $imageUuid = $request->request->get('uuid');

        $imageGallery = $galleryImageRepository->findOneBy(['uuid' => $imageUuid]);
        $imageGalleryForm = $this->createForm(GalleryImageFormType::class, $imageGallery);

        $html = $this->renderView(
            '@FlagstoneGallery/image_info.html.twig',
            [
                'edit_image_form'    => $imageGalleryForm->createView(),
                'title'                 => 'Edit image information'
            ]
        );

        return new JsonResponse(
            [
                'status'    => 'ok',
                'html'      => $html
            ]
        );
    }

    #[Route(path: '/save-gallery-image', name: 'save_gallery_image', methods: 'POST')]
    public function saveGalleryImage(Request $request, GalleryImageRepository $galleryImageRepository, Saver $saver): JsonResponse
    {
        $formData = $request->request->all('gallery_image_form');

        $uuid = $formData['uuid'];

        $imageGallery = $galleryImageRepository->findOneBy(['uuid' => $uuid]);
        $imageGalleryForm = $this->createForm(GalleryImageFormType::class, $imageGallery);

        $imageGalleryForm->handleRequest($request);

        if ($imageGalleryForm->isValid() && $imageGalleryForm->isSubmitted()) {
            try {
                $saver->save($imageGallery);
                return new JsonResponse(
                    [
                        'status'    => 'ok'
                    ]
                );
            } catch (SaverSaveException $e) {
                return new JsonResponse(
                    [
                        'status'    => 'failed',
                        'message'   => 'unable to save image information'
                    ]
                );
            }
        }

        $html = $this->renderView(
            '@FlagstoneGallery/image_info.html.twig',
            [
                'edit_image_form'   => $imageGalleryForm->createView(),
                'title'             => 'Edit image information'
            ]
        );

        return new JsonResponse(
            [
                'status'    => 'failed',
                'html'      => $html
            ]
        );
    }

    private function removeFilesFromFolder(string $folder): void
    {
        $this->removeFiles($folder);
    }

    private function removeFiles(string $fileOrDirectory): void
    {
        if (is_file($fileOrDirectory)) {
            unlink($fileOrDirectory);
            return;
        }

        $scan = glob(rtrim($fileOrDirectory, '/').'/*');
        foreach($scan as $index => $path) {
            $this->removeFiles($path);
        }

        if ($fileOrDirectory !== $this->getParameter('kernel.project_dir').'/public/uploads/tmp') {
            @rmdir($fileOrDirectory);
        }
    }


    private function addImageFiles(array &$files, string $filename): void
    {
        $newFile = new File($this->getTmpPath().'/'.$filename);

        if (in_array($newFile->getExtension(), self::AVAILABLE_IMAGE_EXTENSIONS)) {
            /* Ajout du fichier dans la liste des fichiers à traiter */
            $files[] = $filename;
        } else {
            unlink($this->getTmpPath().'/'.$filename);
        }
    }

    private function getTmpPath(): string
    {
        $tmpPath = $this->getParameter('kernel.project_dir').'/public/'.$this->getParameter('tmp_path');
        if (!file_exists($tmpPath)) {
            @mkdir($tmpPath, 0777, true);
        }

        return $tmpPath;
    }

    private function getThumbnailsPath(string $galleryUuid): string
    {
        $thumbnailsPath = $this->getParameter('kernel.project_dir').'/public/'.$this->getParameter('thumbnails_path').'/'.$galleryUuid;
        if (!file_exists($thumbnailsPath)) {
            @mkdir($thumbnailsPath, 0777, true);
        }

        return $thumbnailsPath;
    }

    private function getThumbnailsUri(string $galleryUuid): string
    {
        $thumbnailsPath = '/'.$this->getParameter('thumbnails_path').'/'.$galleryUuid;
        if (!file_exists($thumbnailsPath)) {
            @mkdir($thumbnailsPath, 0777, true);
        }

        return $thumbnailsPath;
    }

    private function getImagesPath(string $galleryUuid): string
    {
        $imagesPath = $this->getParameter('kernel.project_dir').'/public/'.$this->getParameter('images_path').'/'.$galleryUuid;
        if (!file_exists($imagesPath)) {
            @mkdir($imagesPath, 0777, true);
        }

        return $imagesPath;
    }

    private function getImageFormat(): string
    {
        return $this->getParameter('image_format');
    }

    private function getThumbnailFormat(): string
    {
        return $this->getParameter('thumbnail_format');
    }

    private function getThumbnailMaxWidth(): string
    {
        return $this->getParameter('thumbnail_max_width');
    }

    private function getImageMaxWidth(): string
    {
        return $this->getParameter('image_max_width');
    }
}