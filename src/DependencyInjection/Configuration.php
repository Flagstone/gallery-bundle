<?php

namespace Flagstone\GalleryBundle\DependencyInjection;

use Symfony\Component\Config\Definition\Builder\TreeBuilder;
use Symfony\Component\Config\Definition\ConfigurationInterface;

class Configuration implements ConfigurationInterface
{
    public function getConfigTreeBuilder(): TreeBuilder
    {
        $builder = new TreeBuilder('flagstone_gallery');

        $rootNode = $builder->getRootNode();
        $rootNode
            ->children()
                ->scalarNode('tmp_path')
                    ->defaultValue('uploads/gallery/images')
                ->end()
            ->end();

        return $builder;
    }
}