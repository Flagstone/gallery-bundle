<?php

namespace Flagstone\GalleryBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Flagstone\BaseEntityBundle\BaseEntity\AbstractMappedSuperclass;
use Flagstone\GalleryBundle\Repository\GalleryImageRepository;
use Flagstone\UuidDoctrineBridgeBundle\UuidDoctrineBridge\Interfaces\UuidInterface;
use Flagstone\UuidDoctrineBridgeBundle\UuidDoctrineBridge\Traits\UuidGenerator;
use Symfony\Component\HttpFoundation\File\File;
use Symfony\Component\HttpFoundation\File\UploadedFile;

#[ORM\Entity(repositoryClass: GalleryImageRepository::class)]
#[ORM\Table(name: "gallery_image")]
class GalleryImage extends AbstractMappedSuperclass implements UuidInterface
{
    use UuidGenerator;

    #[ORM\Column(name: 'gallery_image_name', type: 'string', length: 63, nullable: true)]
    private ?string $galleryImageName;

    #[ORM\Column(name: 'gallery_original_image_name', type: 'string', length: 127, nullable: true)]
    private ?string $galleryOriginalImageName;

    #[ORM\Column(name: 'gallery_image_description', type: 'text', length: 1000, nullable: true)]
    private ?string $galleryImageDescription;

    #[ORM\Column(name: 'gallery_image_filename', type: 'string', length: 255, nullable: false)]
    private string $galleryImageFilename;

    #[ORM\ManyToOne(targetEntity: Gallery::class, inversedBy: 'galleryImages' )]
    #[ORM\JoinColumn(name: 'gallery_id', referencedColumnName: 'id', nullable: false)]
    private ?Gallery $gallery;

    /* No Mapping */
    private null|UploadedFile|File $galleryImageFile;

    public function getGalleryImageName(): ?string
    {
        return $this->galleryImageName;
    }

    public function getGalleryOriginalImageName(): ?string
    {
        return $this->galleryOriginalImageName;
    }

    public function getGalleryImageDescription(): ?string
    {
        return $this->galleryImageDescription;
    }

    public function getGalleryImageFilename(): string
    {
        return $this->galleryImageFilename;
    }

    public function getGallery(): ?Gallery
    {
        return $this->gallery;
    }

    public function getGalleryImageFile(): null|UploadedFile|File
    {
        return $this->galleryImageFile;
    }

    public function setGalleryImageName(?string $galleryImageName): self
    {
        $this->galleryImageName = $galleryImageName;
        return $this;
    }

    public function setGalleryOriginalImageName(?string $galleryOriginalImageName): self
    {
        $this->galleryOriginalImageName = $galleryOriginalImageName;
        return $this;
    }

    public function setGalleryImageDescription(?string $galleryImageDescription): self
    {
        $this->galleryImageDescription = $galleryImageDescription;
        return $this;
    }

    public function setGalleryImageFilename(?string $galleryImageFilename): self
    {
        $this->galleryImageFilename = $galleryImageFilename;
        return $this;
    }

    public function setGallery(?Gallery $gallery): self
    {
        $this->gallery = $gallery;
        return $this;
    }

    public function setGalleryImageFile(null|UploadedFile|File $file): self
    {
        $this->galleryImageFile = $file;
        return $this;
    }
}