<?php

namespace Flagstone\GalleryBundle\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Flagstone\BaseEntityBundle\BaseEntity\AbstractMappedSuperclass;
use Flagstone\UuidDoctrineBridgeBundle\UuidDoctrineBridge\Interfaces\UuidInterface;
use Flagstone\UuidDoctrineBridgeBundle\UuidDoctrineBridge\Traits\UuidGenerator;

#[ORM\Entity()]
#[ORM\Table(name: "gallery")]
class Gallery extends AbstractMappedSuperclass implements UuidInterface
{
    use UuidGenerator;

    #[ORM\Column(name: 'gallery_name', type: 'string', length: 63, nullable: false)]
    private string $galleryName;
    #[ORM\Column(name: 'gallery_description', type: 'text', length: 1000, nullable: true)]
    private ?string $galleryDescription;

    #[ORM\OneToMany(mappedBy: 'gallery', targetEntity: GalleryImage::class, cascade: ['persist'], orphanRemoval: true)]
    private Collection $galleryImages;

    public function __construct()
    {
        $this->galleryImages = new ArrayCollection();
    }

    public function getGalleryName(): string
    {
        return $this->galleryName;
    }

    public function getGalleryDescription(): ?string
    {
        return $this->galleryDescription;
    }

    public function getGalleryImages(): Collection
    {
        return $this->galleryImages;
    }

    public function setGalleryName(string $galleryName): self
    {
        $this->galleryName = $galleryName;
        return $this;
    }

    public function setGalleryDescription(?string $galleryDescription): self
    {
        $this->galleryDescription = $galleryDescription;
        return $this;
    }

    public function addGalleryImage(?GalleryImage $image): self
    {
        if (null !== $image) {
            $this->galleryImages->add($image);
            $image->setGallery($this);
        }
        return $this;
    }

    public function removeGalleryImage(?GalleryImage $image): self
    {
        if (null !== $image) {
            $this->galleryImages->removeElement($image);
            $image->setGallery(null);
        }
        return $this;
    }
}