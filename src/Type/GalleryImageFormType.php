<?php

namespace Flagstone\GalleryBundle\Type;

use Flagstone\GalleryBundle\Entity\GalleryImage;
use FOS\CKEditorBundle\Form\Type\CKEditorType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\HiddenType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Validator\Constraints\Length;

class GalleryImageFormType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add(
                'uuid',
                HiddenType::class
            )
            ->add(
                'galleryImageName',
                TextType::class,
                [
                    'required'  => true,
                    'attr'      => [
                        'class'     => 'form-control'
                    ],
                    'label_attr'    => [
                        'class'         => 'mt-2'
                    ],
                    'constraints'   => [
                        new Length([
                                'max'           => 63,
                                'maxMessage'    => 'Maximum 63 characters'
                            ]
                        )
                    ]
                ]
            )
            ->add(
                'galleryImageDescription',
                CKEditorType::class,
                [
                    'config_name'   => 'description_admin',
                    'required'      => false,
                    'attr'          => [
                        'class'         => 'w-100 form-control'
                    ],
                    'label_attr'    => [
                        'class'         => 'mt-2'
                    ]
                ]
            );
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(
            [
                'data_class'        => GalleryImage::class,
                'attr'              => [
                    'id'                        => 'gallery-image-form'
                ]
            ]
        );
    }
}