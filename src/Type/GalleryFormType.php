<?php

namespace Flagstone\GalleryBundle\Type;

use Flagstone\CmsBundle\Service\SiteConfiguration;
use Flagstone\GalleryBundle\Entity\Gallery;
use FOS\CKEditorBundle\Form\Type\CKEditorType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\FileType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\HiddenType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\HttpFoundation\RequestStack;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Routing\Generator\UrlGeneratorInterface;
use Symfony\Component\Routing\RouterInterface;
use Symfony\Component\DependencyInjection\ParameterBag\ParameterBagInterface;
use Symfony\Component\Validator\Constraints\File;

class GalleryFormType extends AbstractType
{
    public function __construct(private UrlGeneratorInterface $urlGenerator)
    {
    }

    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $mode = $options['mode'];

        $builder
            ->setAction($this->urlGenerator->generate('gallery_save'))
            ->add(
                'files',
                FileType::class,
                [
                    'required'  => false,
                    'label'     => false,
                    'multiple'  => true,
                    'mapped'    => false,
                    'attr'  => [
                        'style'     => 'display: none',
                        'class'     => 'upload',
                        'accept'    => '.jpg,.jpeg,.png,.webp,.zip',
                        'multiple'  => 'multiple'

                    ]
                ]

            )
            ->add(
                'uuid',
                HiddenType::class
            )
            ->add(
                'galleryName',
                TextType::class,
                [
                    'required'  => true,
                    'attr'      => [
                        'class'     => 'form-control'
                    ],
                    'label_attr'    => [
                        'class'         => 'mt-2'
                    ]
                ]
            )
            ->add(
                'galleryDescription',
                CKEditorType::class,
                [
                    'config_name'   => 'description_admin',
                    'required'      => false,
                    'attr'          => [
                        'class'         => 'w-100 form-control'
                    ],
                    'label_attr'    => [
                        'class'         => 'mt-2'
                    ]
                ]
            );
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(
            [
                'data_class'        => Gallery::class,
                'attr'              => [
                    'id'                        => 'gallery-form'
                ],
                'mode'              => null
            ]
        );
    }
}